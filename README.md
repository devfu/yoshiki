## Yōshiki (様式)

# Dev Fu! Style

[![rubygems][gem-image]][gem] [![build status][ci-image]][ci]

Because ruby code should be:

- Readable.
- Elegant.
- Easily read by humans.
- Approachable by newbs.
- Clear to developers.
- Profound to rubyists.

Make it so.

## Usage

In your gemfile

`gem 'yoshiki'`


In `.rubocop.yml`

```
inherit_gem:
  yoshiki: .yoshiki.yml
```

to include just ruby

```
inherit_gem:
  yoshiki: .yoshiki-ruby.yml
```

or, to include just rails

```
inherit_gem:
  yoshiki: .yoshiki-rails.yml
```

## Ruby compatibility

- ruby 3.4+, use [~> 10.0][10.0]
- ruby 3.0+, use [~> 9.0][9.0]
- ruby 2.7, use [~> 8.0][8.0]
- ruby 2.6, use [~> 7.0][7.0]
- ruby 2.4+, use [~> 6.0][6.0.1]
- ruby 2.3, use [~> 3.2][3.2.0]

<!-- links -->
[ci]: https://gitlab.com/devfu/yoshiki/pipelines?scope=branches "build history"
[gem]: https://rubygems.org/gems/yoshiki
[3.2.0]: https://gitlab.com/devfu/yoshiki/tags/v3.2.0
[6.0.1]: https://gitlab.com/devfu/yoshiki/tags/v6.0.1
[7.0]: https://gitlab.com/devfu/yoshiki/tags/v7.0.0
[8.0]: https://gitlab.com/devfu/yoshiki/tags/v8.0.0
[9.0]: https://gitlab.com/devfu/yoshiki/tags/v9.0.0
[9.0]: https://gitlab.com/devfu/yoshiki/tags/v10.0.0

<!-- images -->
[ci-image]: https://gitlab.com/devfu/yoshiki/badges/master/pipeline.svg
[gem-image]: https://badge.fury.io/rb/yoshiki.svg
