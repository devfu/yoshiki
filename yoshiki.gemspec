lib = File.expand_path 'lib', __dir__
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'yoshiki/version'

Gem::Specification.new do |spec|
  spec.name          = 'yoshiki'
  spec.version       = Yoshiki::VERSION
  spec.authors       = [ 'BM5k' ]
  spec.email         = %w[ me@bm5k.com ]
  spec.summary       = 'Dev Fu! Style'
  spec.description   = 'Dev Fu! Style for rubocop.'
  spec.homepage      = 'https://gitlab.com/devfu/yoshiki'
  spec.license       = 'MIT'
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match %r{^(test|spec|features)/} }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename f }
  spec.require_paths = %w[ lib ]

  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.required_ruby_version = '>= 3.4.0'

  spec.add_dependency 'rubocop',             '1.72.2'
  spec.add_dependency 'rubocop-capybara',    '2.21.0'
  spec.add_dependency 'rubocop-factory_bot', '2.26.1'
  spec.add_dependency 'rubocop-performance', '1.24.0'
  spec.add_dependency 'rubocop-rails',       '2.30.1'
  spec.add_dependency 'rubocop-rake',        '0.7.1'
  spec.add_dependency 'rubocop-rspec',       '3.5.0'
  spec.add_dependency 'rubocop-rspec_rails', '2.30.0'
end
