# Code here should not cause rubocop to nag
class Examples

  # rubocop:disable Layout/CommentIndentation,Style/AccessModifierDeclarations
  private def example_for_layout_def_end_alignment
            # this method is *not* empty
          end
  # rubocop:enable Layout/CommentIndentation, Style/AccessModifierDeclarations

  def example_for_layout_else_alignment
    if example_for_layout_def_end_alignment
      # do_something
      else # rubocop:disable Layout/ElseAlignment, Style/EmptyElse
      # do_something_else
    end
  end

  def example_for_layout_end_alignment
    foo = begin
            'foo'
          rescue e
            # do nothing
          end

    "#{ foo } bar"
  end

end
