FROM ruby:alpine

RUN apk upgrade --update && apk add --no-cache git build-base

ENV app /src
RUN mkdir $app
WORKDIR $app
ADD . $app

RUN bundle install

CMD bundle console
